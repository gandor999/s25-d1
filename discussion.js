db.fruits.insertMany(
    [
        {
            "name": "Apple",
            "color": "Red",
            "stock": 20,
            "price": 40,
            "supplier_id": 1,
            "onSale": true,
            "origin": [
                "Philippines", "US"
            ]
        },
        {
            "name": "Banana",
            "color": "Yellow",
            "stock": 15,
            "price": 20,
            "supplier_id": 2,
            "onSale": true,
            "origin": [
                "Philippines", "Ecuador"
            ]
        },
        {
            "name": "Kiwi",
            "color": "Green",
            "stock": 25,
            "price": 50,
            "supplier_id": 1,
            "onSale": true,
            "origin": [
                "US", "China"
            ]
        },
        {
            "name": "Mango",
            "color": "Yellow",
            "stock": 10,
            "price": 120,
            "supplier_id": 2,
            "onSale": false,
            "origin": [
                "Philippines", "India"
            ]
        }
    ]
);

// MongoDB Aggregation
// - used to generate manipluated data and perform operations to create filtered results that help in analyizing data


// Using the Aggregate Method
/*
    $match
        - used to pass documents that meet the specified conditions top the next pipeline stage/aggregation process

        syntax: { $match: {field: value} }


    $group
        - used to group elements together and field-value pairs using the data from the grouped elements

        syntax: { $group: { _id: "value", fieldResult: "valueResult" } }

*/

db.fruits.aggregate([
    {   
        $match: { "onSale": true }
    },
    {
        $group: { "_id": "$supplier_id", "total": { $sum: "$stock" } }
    }
]);




// Sorting aggregated results
/*
    $sort
        - can be used to change the order of aggregated result

        syntax: { $sort: { field: 1/-1 } }
*/

db.fruits.aggregate([
    {
        $match: { "onSale": true }
    },
    {
        $group: { "_id": "$supplier_id", "total": { $sum: "$stock" } }
    },
    {
        $sort: { "total": -1 }
    }
]);


// Aggregating resulst based on array fields
/*
    $unwind
        - deconstructs an array field from a collection/field with an array value to output a result for each element

        syntax: { $unwind: field-that-holds-array }
*/

db.fruits.aggregate([
    {
        $unwind:  "$origin"
    }
]);

db.fruits.aggregate([
    {
        $unwind:  "$origin"
    },
    {
        $group: { "_id": "$origin", kinds: { $sum: 1 } }
    }
]);





// Schema Design

// One-to-One Relationship

var owner = ObjectId();

db.owners.insert({
    _id: owner,
    name: "John Smith",
    contact: "09876543218"
});

db.suppliers.insert({
    name: "ABC Fruits",
    contact: "09998876543",
    owner_id: <owner_id>
});

db.suppliers.insert({
    name: "ABC Fruits",
    contact: "09998876543",
    owner_id: ObjectId("6177709df7724ebcd854a8fa")
});


// Update the owner document and insert the new field
db.owners.updateOne(
    {
        "_id": ObjectId("6177709df7724ebcd854a8fa")
    },
    {
        $set: {
            supplier_id: ObjectId("6177717bf7724ebcd854a8fb")
        }
    }
);


// One-to-Many Relationship
db.suppliers.insertMany([
        {
            name: "DEF Fruits",
            contact: "09998876456",
            owner_id: ObjectId("6177709df7724ebcd854a8fa")
        },
        {
            name: "GHI Fruits",
            contact: "09998876130",
            owner_id: ObjectId("6177709df7724ebcd854a8fa")
        }
    ]
);




// if ma bantayan nimo walay array brackets tang gi butang diri for id and set na objects. seems as though na di na ka kailangan ug brackets para sa filter which is the id object sa una
db.owners.updateOne(
    {
        "_id": ObjectId("6177709df7724ebcd854a8fa")
    },
    {
        $set: {
            supplier_id: [
                ObjectId("617779c6f7724ebcd854a8fc"),
                ObjectId("617779c6f7724ebcd854a8fd")
            ]
        }
    }
);
